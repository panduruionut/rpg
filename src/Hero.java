public class Hero extends Behaviour{
    static String NAME;
    static int HP;
     static int ATTACK_STRENGTH;
    private int DEFENSE;
    static int EXP=0;

    public Hero(String name,int hp,int attack_strength,int defense){
        super(name,hp,attack_strength,defense);
    NAME=name;
    HP=hp;
    ATTACK_STRENGTH=attack_strength;
    DEFENSE=defense;
        System.out.println("Created hero  "+NAME+".   "+" HP= "+HP+" ATK= "+ATTACK_STRENGTH+" EXP="+EXP);
    }
    public static void setHP(int hp){
        HP=hp;
    }
    public static String getName() {
        return NAME;
    }
    public int showHP(){
        System.out.println(NAME+" hp left: "+HP);
        return this.HP;
    }

}
