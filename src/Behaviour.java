public class Behaviour {
    private int HP;
    private int ATTACK_STRENGTH;
    private int DEFENSE;
    private String NAME;
    private int EXP;
    private String character;
    public Behaviour(String name,int hp,int attack_strength,int defense){
        NAME=name;
        HP=hp;
        ATTACK_STRENGTH=attack_strength;
        DEFENSE=defense;
        EXP=0;
    }

    public void attack(String character) {
        int value = 0;
        if (character == Monster.getName() && Monster.HP > 0 ) {
            value = (Monster.HP - Hero.ATTACK_STRENGTH);
            Monster.setHP(value);
            System.out.println(this.NAME + "     attacked    " + character + "   for     " + ATTACK_STRENGTH + "     DMG");
        }else if(character == Hero.getName() && Hero.HP > 0){
            int val=(Hero.HP-Monster.ATTACK_STRENGTH);
            Hero.setHP(val);
            System.out.println(this.NAME + "     attacked    " + character + "   for     " + ATTACK_STRENGTH + "     DMG");

        }
            if (Monster.HP == 0) {
                die();
            }


        }



    public void die(){
        System.out.println(this.NAME+"Got killed and lost it's experience to:"+Hero.NAME);
    }
    public static void main(String[] args){
        Hero hero=new Hero("wyn",200,50,5);
        Monster wildBear=new Monster("wildBear",300,10,3,10);
        wildBear.showInfo();
        wildBear.showHP();
        hero.attack("wildBear");
        wildBear.showHP();
        wildBear.attribute("rockskin");
        hero.attack("wildBear");
        wildBear.showHP();
        hero.showHP();
        wildBear.attack("wyn");
        hero.showHP();
    }
}
