public class Monster extends Behaviour {
    static String NAME;
    static int HP;
    static int ATTACK_STRENGTH;
    int DEFENSE;
    static int EXP;

    public Monster(String name, int hp, int attack_strength, int defense,int exp) {
        super(name, hp, attack_strength, defense);
        NAME = name;
        HP = hp;
        ATTACK_STRENGTH = attack_strength;
        DEFENSE = defense;
        EXP=exp;
        System.out.println("Created monster of race "+NAME+".   "+" HP= "+HP+" ATK= "+ATTACK_STRENGTH);
    }
    public void showInfo(){
        System.out.println(NAME+" has "+HP+" hp ,"+ATTACK_STRENGTH+" attack strength ,"+DEFENSE+ " defense");
    }
    public static void setHP(int hp) {
        HP = hp;
    }

    public static String getName() {
        return NAME;
    }

    public int showHP() {
        if (HP > 0){
            System.out.println(NAME + "   HP left:    " + HP);
    }else

    {

        System.out.println(NAME + "  Got killed by  " + Hero.NAME+" and received "+EXP+" experience");
        Hero.EXP=Hero.EXP+EXP;
        System.exit(0);
    }

return HP;
}
 public void attribute(String s){
    if(s=="rockskin"){
        System.out.println(NAME+" activated it's attribute:rockskin");
        Hero.ATTACK_STRENGTH=Hero.ATTACK_STRENGTH-5;
    }
 }


    }
